package com.dexciuq.notesapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.dexciuq.notesapp.adapter.NoteAdapter
import com.dexciuq.notesapp.databinding.FragmentMainBinding
import com.dexciuq.notesapp.db.AppDatabase
import com.dexciuq.notesapp.viewmodel.MainViewModel
import com.dexciuq.notesapp.viewmodel.factory.NoteRepository
import com.dexciuq.notesapp.viewmodel.factory.NoteViewModelFactory

class MainFragment : Fragment() {

    private lateinit var adapter: NoteAdapter
    private val binding by lazy { FragmentMainBinding.inflate(layoutInflater) }
    private val navController by lazy { findNavController() }
    private val viewModel: MainViewModel by viewModels {
        NoteViewModelFactory(
            NoteRepository(AppDatabase.getInstance(requireContext()).noteDao())
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        adapter = NoteAdapter {
            val action = MainFragmentDirections.actionMainToDetails(it.id)
            navController.navigate(action)
        }

        binding.noteRecyclerView.adapter = adapter
        binding.noteRecyclerView.layoutManager = LinearLayoutManager(activity)

        binding.create.setOnClickListener {
            navController.navigate(R.id.action_main_to_edit)
        }
        viewModel.notes.observe(viewLifecycleOwner, adapter::submitList)
        return binding.root
    }

}