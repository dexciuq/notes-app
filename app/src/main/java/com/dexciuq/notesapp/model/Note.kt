package com.dexciuq.notesapp.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize
import java.util.Date

@Parcelize
@Entity(tableName = "notes")
data class Note (
    val name: String,
    var note: String,
    @ColumnInfo(name = "modified_date") var modifiedDate: Date = Date(),
    @PrimaryKey(autoGenerate = true) val id: Long = 0
) : Parcelable