package com.dexciuq.notesapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dexciuq.notesapp.model.Note
import com.dexciuq.notesapp.viewmodel.factory.NoteRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel(
    repository: NoteRepository
) : ViewModel() {

    val notes: LiveData<List<Note>> = repository.notes
}