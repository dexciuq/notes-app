package com.dexciuq.notesapp.viewmodel.factory

import androidx.lifecycle.LiveData
import com.dexciuq.notesapp.db.dao.NoteDao
import com.dexciuq.notesapp.model.Note

class NoteRepository(private val noteDao: NoteDao) {
    val notes: LiveData<List<Note>> = noteDao.getAll()

    suspend fun get(noteId: Long) = noteDao.get(noteId)

    suspend fun insert(note: Note) = noteDao.insert(note)

    suspend fun delete(note: Note) = noteDao.delete(note)

    suspend fun update(note: Note) = noteDao.update(note)
}