package com.dexciuq.notesapp.viewmodel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dexciuq.notesapp.viewmodel.DetailsViewModel
import com.dexciuq.notesapp.viewmodel.EditViewModel
import com.dexciuq.notesapp.viewmodel.MainViewModel

@Suppress("UNCHECKED_CAST")
class NoteViewModelFactory(private val repository: NoteRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(MainViewModel::class.java) -> MainViewModel(repository) as T
            modelClass.isAssignableFrom(EditViewModel::class.java) -> EditViewModel(repository) as T
            modelClass.isAssignableFrom(DetailsViewModel::class.java) -> DetailsViewModel(repository) as T
            else -> error("Unknown ViewModel class: ${modelClass.simpleName}")
        }
    }
}