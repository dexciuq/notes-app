package com.dexciuq.notesapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dexciuq.notesapp.model.Note
import com.dexciuq.notesapp.viewmodel.factory.NoteRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class EditViewModel(
    private val repository: NoteRepository
) : ViewModel() {

    private val _note = MutableLiveData<Note>()
    val note : LiveData<Note> = _note

    fun get(noteId: Long) = viewModelScope.launch {
        val item = repository.get(noteId)
        _note.postValue(item)
    }

    fun insert(note: Note) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(note)
    }

    fun update(note: Note) = viewModelScope.launch(Dispatchers.IO) {
        repository.update(note)
    }
}