package com.dexciuq.notesapp

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.dexciuq.notesapp.databinding.FragmentEditBinding
import com.dexciuq.notesapp.db.AppDatabase
import com.dexciuq.notesapp.model.Note
import com.dexciuq.notesapp.viewmodel.EditViewModel
import com.dexciuq.notesapp.viewmodel.factory.NoteRepository
import com.dexciuq.notesapp.viewmodel.factory.NoteViewModelFactory
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase

class EditFragment : Fragment() {

    private val binding by lazy { FragmentEditBinding.inflate(layoutInflater) }
    private val navController by lazy { findNavController() }
    private val args: EditFragmentArgs by navArgs()
    private val viewModel: EditViewModel by viewModels {
        NoteViewModelFactory(
            NoteRepository(AppDatabase.getInstance(requireActivity()).noteDao())
        )
    }
    private lateinit var analytics: FirebaseAnalytics

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        analytics = Firebase.analytics
        val id = args.noteId
        if (id != -1L) viewModel.get(id)

        viewModel.note.observe(viewLifecycleOwner, ::setupView)
        setupSaveButton(id)
        return binding.root
    }

    private fun setupView(item: Note) {
        binding.editName.isEnabled = false
        binding.editName.setText(item.name)
        binding.editNote.setText(item.note)
    }

    private fun setupSaveButton(id: Long) {
        binding.save.setOnClickListener {
            if (id == -1L) {
                val note = Note(
                    name = binding.editName.text.toString(),
                    note = binding.editNote.text.toString(),
                )
                viewModel.insert(note)
                val bundle = bundleOf("note_id" to note.id, "note_name" to note.name)
                analytics.logEvent("add_note", bundle)
            } else {
                val note = Note(
                    id = id,
                    name = binding.editName.text.toString(),
                    note = binding.editNote.text.toString(),
                )
                viewModel.update(note)
            }
            navController.navigate(R.id.action_edit_to_main)
        }
    }
}