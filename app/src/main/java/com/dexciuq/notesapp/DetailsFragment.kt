package com.dexciuq.notesapp

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.dexciuq.notesapp.databinding.FragmentDetailsBinding
import com.dexciuq.notesapp.db.AppDatabase
import com.dexciuq.notesapp.model.Note
import com.dexciuq.notesapp.viewmodel.DetailsViewModel
import com.dexciuq.notesapp.viewmodel.factory.NoteRepository
import com.dexciuq.notesapp.viewmodel.factory.NoteViewModelFactory
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class DetailsFragment : Fragment() {

    private val binding by lazy { FragmentDetailsBinding.inflate(layoutInflater) }
    private val navController by lazy { findNavController() }
    private val args: DetailsFragmentArgs by navArgs()
    private val viewModel: DetailsViewModel by viewModels {
        NoteViewModelFactory(
            NoteRepository(AppDatabase.getInstance(requireActivity()).noteDao())
        )
    }
    private lateinit var note: Note
    private lateinit var analytics: FirebaseAnalytics

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        analytics = Firebase.analytics
        viewModel.get(args.noteId)

        binding.edit.setOnClickListener {
            val action = DetailsFragmentDirections.actionDetailsToEdit(note.id)
            navController.navigate(action)
        }

        binding.delete.setOnClickListener {
            val bundle = bundleOf("note_id" to note.id, "note_name" to note.name,)
            analytics.logEvent("remove_note", bundle)
            viewModel.delete(note)
            navController.navigate(R.id.action_details_to_main)
        }

        viewModel.note.observe(viewLifecycleOwner) {
            note = it
            binding.name.text = "Name: ${it.name}"
            binding.note.text = "Note: ${it.note}"
            val bundle = bundleOf("note_id" to it.id, "note_name" to it.name)
            analytics.logEvent("view_note", bundle)
        }

        return binding.root
    }
}