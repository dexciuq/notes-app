package com.dexciuq.notesapp.db.converter

import androidx.room.TypeConverter
import java.util.Date

class DateConverter {
    @TypeConverter
    fun fromTimestampToDate(timestamp: Long) : Date = Date(timestamp)

    @TypeConverter
    fun fromDateToTimestamp(date: Date) : Long = date.time
}