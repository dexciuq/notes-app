package com.dexciuq.notesapp.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.dexciuq.notesapp.model.Note
import com.dexciuq.notesapp.db.converter.DateConverter
import com.dexciuq.notesapp.db.dao.NoteDao

@Database(entities = [Note::class], version = 1)
@TypeConverters(DateConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun noteDao(): NoteDao

    companion object {
        private var instance: AppDatabase? = null

        fun getInstance(context: Context) : AppDatabase = instance ?: synchronized(AppDatabase::class) {
            Room.databaseBuilder(
                context = context.applicationContext,
                klass = AppDatabase::class.java,
                name = "notesapp.db"
            ).build().also { instance = it }
        }
    }
}